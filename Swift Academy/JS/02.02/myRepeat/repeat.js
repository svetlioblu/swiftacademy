function selectList(selectors) {
    return document.querySelectorAll(selectors);

}

function loopElements() {
    var myElements = selectList('ul li');
    for (var i = 0; i < myElements.length; i += 1) {
        // console.log(myElements[i].innerHTML);
        myElements[i].addEventListener('click', function() {

            this.style.backgroundColor = 'red';
        }, false);
        myElements[i].addEventListener('mouseover', function() {
            this.style.marginLeft = '15px';
        }, false);

    }
}

function changeColor() {
    var input = document.querySelectorAll('#myInput');
    var element = document.querySelectorAll('body');


    input[0].addEventListener('keyup', function() {
        var colors = ['orange', 'green', 'blue', 'lightgreen', 'yellow', 'pink', 'white', 'purple'];
        var random = Math.floor(Math.random() * colors.length - 1) + 1;
        element[0].style.backgroundColor = colors[random];
    }, false);
    input[0].addEventListener('click', function() {
        var x = 'blue';
        this.style.backgroundColor = x;
    }, false);

}

window.addEventListener('load', function() {
    loopElements();
    changeColor();

}, false);
