function createDiv() {
    var div = document.createElement('div');
    var isDown;
    div.style.width = '150px';
    div.style.height = '150px';
    div.style.backgroundColor = 'lime';
    div.style.position = 'absolute';
    div.style.top = '0px';
    div.style.left = '0px';
    document.body.appendChild(div);
    div.addEventListener('mousedown', function() {
        isDown = true;

    }, false);
    div.addEventListener('mouseup', function() {
        isDown = false;
    }, false);
    document.addEventListener('mousemove', function(event) {
        event.preventDefault();


        var move = {
            x: event.clientX,
            y: event.clientY
        };
        if (isDown) {
            div.style.left = move.x + 'px';
            div.style.top = move.y + 'px';

        }
    }, false);

}



window.addEventListener('load', function() {
    createDiv();

}, false);
